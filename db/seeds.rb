# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'csv'

csv_text = File.read(Rails.root.join('au-towns-sample.csv'))
suburbs = CSV.parse(csv_text, headers: true)
suburbs.each do |row|
  Suburb.create(name: row['name'].upcase, state: row['state_code'], postcode: row['postcode'].to_i)
end
