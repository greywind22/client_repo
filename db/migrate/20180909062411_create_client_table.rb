class CreateClientTable < ActiveRecord::Migration[5.1]
  def change
    create_table :clients do |t|
      t.string   "email",   limit: 255
      t.string   "first_name",    limit: 255
      t.string   "last_name",    limit: 255
    end
  end
end
