class CreateAddressTable < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.string 'street'
      t.string 'locality'
      t.string 'state'
      t.integer 'postcode', limit: 4
      t.string 'address_type', limit: 10
      t.boolean 'address_valid'
    end
  end
end
