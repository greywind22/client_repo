class CreateSuburbsTable < ActiveRecord::Migration[5.1]
  def change
    create_table :suburbs do |t|
      t.string 'name'
      t.string 'state'
      t.integer 'postcode', limit: 4
    end
  end
end
