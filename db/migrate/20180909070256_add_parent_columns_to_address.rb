class AddParentColumnsToAddress < ActiveRecord::Migration[5.1]
  def change
    add_column :addresses, :parent_id, :integer
    add_column :addresses, :parent_type, :string
  end
end

