require 'rails_helper'

RSpec.describe Address, type: :model do
  describe 'validations' do
    let(:client) { Client.create(first_name: 'Wyman', last_name: 'Manderly', email: 'wyman@whiteharbour.com')}
    let(:address) { Address.new(street: '136 Rundle Mall', locality: 'Adelaide',
                                postcode: 5000, state: 'SA', address_type: 'POSTAL',
                                parent: client) }

    it 'should save for a valid address' do
      expect(address.save).to eql true
    end

    it 'not valid if street is not present' do
      address.street = nil
      expect(address.save).to eql false
      expect(address.errors.messages).to include(:street)
      expect(address.errors.messages[:street].first).to eql 'can\'t be blank'
    end

    it 'not valid if locality is not present' do
      address.locality = nil
      expect(address.save).to eql false
      expect(address.errors.messages).to include(:locality)
      expect(address.errors.messages[:locality].first).to eql 'can\'t be blank'
    end

    it 'not valid if postcode is not present' do
      address.postcode = nil
      expect(address.save).to eql false
      expect(address.errors.messages).to include(:postcode)
      expect(address.errors.messages[:postcode].first).to eql 'can\'t be blank'
    end

    it 'not valid if state is not present' do
      address.state = nil
      expect(address.save).to eql false
      expect(address.errors.messages).to include(:state)
      expect(address.errors.messages[:state].first).to eql 'can\'t be blank'
    end

    it 'should only accept valid states' do
      address.state = 'NA'
      expect(address.save).to eql false
      expect(address.errors.messages).to include(:state)
      expect(address.errors.messages[:state].first).to eql 'is not included in the list'
    end

    it 'not valid if postcode is not 4 digits' do
      address.postcode = 321
      expect(address.save).to eql false
      expect(address.errors.messages).to include(:postcode)
      expect(address.errors.messages[:postcode].first).to eql 'is the wrong length (should be 4 characters)'
    end
  end
end
