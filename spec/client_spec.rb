require 'rails_helper'

RSpec.describe Client, type: :model do

  describe 'validations' do
    it 'should not be valid if no first name' do
      client = Client.new(last_name: 'Snow', email: 'jsnow@castleblack.com')
      expect(client.save).to eql false
      expect(client.errors.messages[:first_name].first).to eql 'can\'t be blank'
    end

    it 'should not be valid if no email' do
      client = Client.new(last_name: 'Snow', first_name: 'Jon')
      expect(client.save).to eql false
      expect(client.errors.messages[:email].first).to eql 'can\'t be blank'
    end

    it 'should be valid if no first name and email present' do
      client = Client.new(first_name: 'Jon', email: 'jsnow@castleblack.com')
      expect(client.save).to eql true
    end

    it 'should validate email format' do
      client = Client.new(first_name: 'Jon', email: 'jsnowcastleblack.com')
      expect(client.save).to eql false
      client = Client.new(first_name: 'Jon', email: 'sam@g@gmail')
      expect(client.save).to eql false
    end
  end
end
