require 'rails_helper'

RSpec.describe ImportClientData do
  describe 'extract data' do
    let(:subject) { ImportClientData.new }

    context 'transpose the row data' do
      let(:first_name) { 'Jon' }
      let(:last_name) { 'Doe' }
      let(:email) { 'jon.doe@gmail.com' }
      let(:street) { '23, South Road' }
      let(:locality) { 'Paramatta' }
      let(:state) { 'NSW' }
      let(:postcode) { '2122' }

      it 'client transpose' do
        client = subject.transpose([email, first_name, last_name], subject.client_fields)
        expect(client[:first_name]).to eql first_name
        expect(client[:last_name]).to eql last_name
        expect(client[:email]).to eql email
      end

      it 'address transpose' do
        address = subject.transpose([street, locality, state, postcode], subject.address_fields)
        expect(address[:street]).to eql street
        expect(address[:locality]).to eql locality
        expect(address[:state]).to eql state
        expect(address[:postcode]).to eql postcode
      end

      it 'should return the extracted data' do
        rows = []
        rows << subject.header
        2.times do
          rows << [email, first_name, last_name, street, locality, state, postcode,
                   street, locality, state, postcode]
        end
        # Drops the header row and has the two client rows
        expect(subject.extract_data(rows).count).to eql 2
      end
    end
  end

  describe 'persist data' do
    let(:street) { '23, South Road' }
    let(:locality) { 'Paramatta' }
    let(:state) { 'NSW' }
    let(:postcode) { '2122' }
    let(:client) { Client.create(first_name: 'Greatjon', last_name: 'Umber', email: 'gjon@lasthearth.com') }

    context 'save client' do
      it 'should update client if email exists' do
        row = { client: { first_name: 'Smalljon', last_name: 'Umber', email: 'gjon@lasthearth.com' },
                residential: { street: street, locality: locality, state: state, postcode: postcode },
                postal: { street: 'Some Other Street', locality: locality, state: state, postcode: postcode }}


        subject.save_client(row)
        clients = Client.where(email: 'gjon@lasthearth.com')
        expect(clients.count).to eql 1
        expect(clients.first.first_name).to eql 'Smalljon'
      end

      it 'should create client if doesnt exist' do
        row = { client: { first_name: 'Barbery', last_name: 'Dustin', email: 'barbery@barrowtown.com' },
                residential: { street: street, locality: locality, state: state, postcode: postcode },
                postal: { street: 'Some Other Street', locality: locality, state: state, postcode: postcode }}

        subject.save_client(row)
        clients = Client.where(email: 'barbery@barrowtown.com')
        expect(clients.count).to eql 1
      end
    end

    context 'save address' do
      before :each do
        @row = { client: { first_name: 'Roose', last_name: 'Bolton', email: 'rbolton@dreadfort.com' },
                 residential: { street: street, locality: locality, state: state, postcode: postcode },
                 postal: { street: 'Flay Street', locality: locality, state: state, postcode: postcode }}
      end
      it 'should create address' do
        subject.save_address(client, @row)
        addresses = Address.all
        expect(addresses.count).to eql 2
        expect(addresses.first.street).to eql '23, South Road'
        expect(addresses.first.parent_id).to eql client.id
        expect(addresses.last.street).to eql 'Flay Street'
        expect(addresses.last.parent_id).to eql client.id
      end

      it 'should mark address as valid if suburb exists' do
        allow(Suburb).to receive(:exists?).and_return(true)
        subject.save_address(client, @row)
        addresses = Address.all
        expect(addresses.count).to eql 2
        expect(addresses.where(parent_id: client.id).count).to eql 2
        expect(addresses.where(address_valid: true).count).to eql 2
      end
    end
  end
end
