require 'CSV'

class ImportClientData

  def self.call
    new.call
  end

  def call
    data = FetchClientData.call
    rows = CSV.parse(data)
    process_client_data(rows)
    UpdateAddressCoordinates.call
  end

  def process_client_data(rows)
    # Bail out if the header is not what we expect.
    return unless header_valid?(rows.first)
    extracted_data = extract_data(rows)
    persist_data(extracted_data)
  end

  def header_valid?(csv_header)
    csv_header == header
  end

  def extract_data(rows)
    data = []
    # Drop the header row
    rows.drop(1).each do |row|
      data.push(client: transpose(row[user_columns], client_fields),
                residential: transpose(row[residential_address_columns], address_fields),
                postal: transpose(row[postal_address_columns], address_fields))
    end
    data
  end

  def persist_data(extracted_data)
    extracted_data.each do |data|
      save_client(data)
    end
  end

  def save_client(data)
    client = Client.find_or_initialize_by(email: data[:client][:email])
    client.assign_attributes(first_name: data[:client][:first_name],
                             last_name: data[:client][:last_name])
    client.save
    # Client update might have been unsuccessful,
    # but don't let that stop from updating address.
    if client.persisted?
      save_address(client, data)
    end
  end

  def save_address(client, data)
    %i[residential postal].each do |address_type|
      address = client.addresses.find_or_initialize_by(address_type: address_type.to_s.upcase)
      addr_data = data[address_type]

      suburb_exists = Suburb.exists?(['lower(name) = ? AND lower(state) = ? AND postcode = ?',
                                      addr_data[:locality]&.downcase,
                                      addr_data[:state]&.downcase,
                                      addr_data[:postcode].to_i])
      address.assign_attributes(addr_data.merge(address_valid: suburb_exists))
      address.save
    end
  end

  def transpose(row, fields)
    attrs = {}
    fields.each_with_index { |key, index| attrs[key] = row[index] }
    attrs
  end

  def client_fields
    %i[email first_name last_name]
  end

  def address_fields
    %i[street locality state postcode]
  end

  def header
    ['Email', 'First Name', 'Last Name',
     'Residential Address Street', 'Residential Address Locality',
     'Residential Address State', 'Residential Address Postcode',
     'Postal Address Street', 'Postal Address Locality',
     'Postal Address State', 'Postal Address Postcode']
  end

  def user_columns
    0..2
  end

  def residential_address_columns
    3..6
  end

  def postal_address_columns
    7..10
  end

  # def self.test
  #   Client.first.update_attributes(first_name: Time.now.to_s)
  # end
end
