class FetchClientData

  def self.call
    new.call
  end

  def call
    uri = URI('http://localhost:3000/client_info/clients_v2.csv')
    req = Net::HTTP::Get.new(uri)
    http = Net::HTTP.new(uri.hostname, uri.port)
    #http.use_ssl = true
    response = http.request(req)
    response.body
  end
end
