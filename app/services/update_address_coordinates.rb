class UpdateAddressCoordinates

  def self.call
    new.call
  end

  def call
    Address.all.each do |address|
      # Don't try to set coordinates for incomplete/invalid addresses.
      next unless address.address_valid?

      uri = URI('https://geocode.xyz')
      params = { locate: address.full_address, auth: '493764112232235476450x864', json: 1 }

      uri.query = URI.encode_www_form(params)
      req = Net::HTTP::Get.new(uri)
      http = Net::HTTP.new(uri.hostname, uri.port)
      http.use_ssl = true
      response = http.request(req)
      parsed = JSON.parse(response.body)

      # Don't update coordinates if we're not confident
      next if parsed['standard']['confidence'].to_f < 0.7
      address.latitude = parsed['latt']
      address.longitude = parsed['longt']
      address.save
    end
  end
end
