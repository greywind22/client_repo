class Address < ApplicationRecord
  belongs_to :parent, polymorphic: true

  validates :street, :locality, presence: true
  validates :postcode, :state, :address_type, presence: true
  validates :parent_id, :parent_type, presence: true
  validates :state, inclusion: { in: %w[WA QLD TAS VIC NSW SA NT ACT] }
  validates :postcode, length: { is: 4 }

  before_validation { |addr| addr.state = addr.state.upcase if addr.state.present? }

  def full_address
    street + ' ' + locality + ' ' + state + ' ' + postcode.to_s
  end
end
