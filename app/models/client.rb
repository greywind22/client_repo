class Client < ApplicationRecord
  validates :first_name, :email, presence: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :email, uniqueness: true

  has_many  :addresses, as: :parent, dependent: :destroy
end
